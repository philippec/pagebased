# PageBased

Exemple basé sur le code d'Apple "Page-Based Application" avec les améliorations suivantes:

* Animation déroulante (scroll) au lieu d'un effet de page (page curl)
* Utilisation d'un UIPageControl indépendant (car il est possible d'en avoir un automatiquement avec UIPageViewController)
* Ajout de deux boutons à gauche et à droite du UIPageControl, afin d'avoir des boutons indépendants de l'animation des pages (par exemple, pour des réglages de l'application)

Ce prototype a été créé pour explorer les possibilités pour la prochaine version de Daylight:

http://apps.casgrain.com/Daylight/
